import { Component } from "react";

class UpDownState extends Component {
    constructor(props) {
        super(props);
        this.state = {
            count: 0
        }
    }

    onButtonUp = () => {
        this.setState({
            count: this.state.count + 1
        })
    }
    onButtonDown = () => {
        this.setState({
            count: this.state.count - 1
        })
    }

    render() {
        return (
            <div>
                <div>
                    Count: {this.state.count}
                </div>
                <div>
                    <button style={{ width: "50px", height: "30px", margin: "5px" }} onClick={this.onButtonUp}>+</button>
                    <button style={{ width: "50px", height: "30px", margin: "5px" }} onClick={this.onButtonDown}>-</button>
                </div>
            </div>

        )
    }
}

export default UpDownState;