import UpDownState from "./components/main";

function App() {
  return (
    <div>
      <UpDownState />
    </div>
  );
}

export default App;
